package com.isw.svcord15.domain.svcord.command;


import org.springframework.stereotype.Service;
import com.isw.svcord15.sdk.domain.svcord.command.Svcord15CommandBase;
import com.isw.svcord15.sdk.domain.svcord.entity.Svcord15Entity;
import com.isw.svcord15.sdk.domain.svcord.entity.ThirdPartyReferenceEntity;
import com.isw.svcord15.sdk.domain.svcord.type.ServicingOrderType;
import com.isw.svcord15.sdk.domain.svcord.type.ServicingOrderWorkProduct;
import com.isw.svcord15.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord15.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord15.sdk.domain.facade.Repository;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord15Command extends Svcord15CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord15Command.class);

  public Svcord15Command(DomainEntityBuilder entityBuilder,  Repository repo ) {
    super(entityBuilder,  repo );
  }

  
    
    @Override
    public com.isw.svcord15.sdk.domain.svcord.entity.Svcord15 createServicingOrderProducer(com.isw.svcord15.sdk.domain.svcord.entity.CreateServicingOrderProducerInput createServicingOrderProducerInput)  {
    
      log.info("Svcord15Command.createServicingOrderProducer()");
      // TODO: Add your command implementation logic

      Svcord15Entity servicingOrderProcedure = this.entityBuilder.getSvcord().getSvcord15().build();
      servicingOrderProcedure.setCustomerReference(createServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessStartDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderType(ServicingOrderType.PAYMENT_CASH_WITHDRAWALS);
      servicingOrderProcedure.setServicingOrderWorkDescription("PAYMENT_CASH_WITHDRAWALS");
      servicingOrderProcedure.setServicingOrderWorkProduct(ServicingOrderWorkProduct.PAYMENT);
      servicingOrderProcedure.setServicingOrderWorkResult(ServicingOrderWorkResult.PROCESSING);

      ThirdPartyReferenceEntity thirdPartyReferenceEntity = new ThirdPartyReferenceEntity();
      thirdPartyReferenceEntity.setId("test1");
      thirdPartyReferenceEntity.setPassword("password");

      servicingOrderProcedure.setThirdPartyReference(thirdPartyReferenceEntity);

      return repo.getSvcord().getSvcord15().save(servicingOrderProcedure);

    }
  
    
    @Override
    public void updateServicingOrderProducer(com.isw.svcord15.sdk.domain.svcord.entity.Svcord15 instance, com.isw.svcord15.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput)  { 

      log.info("Svcord15Command.updateServicingOrderProducer()");
      // TODO: Add your command implementation logic

      Svcord15Entity servicingOrderProcedure = this.repo.getSvcord().getSvcord15().getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
      servicingOrderProcedure.setCustomerReference(updateServicingOrderProducerInput.getCustomerReference());

      servicingOrderProcedure.setProcessEndDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderWorkResult(ServicingOrderWorkResult.SUCCESS);
      servicingOrderProcedure.setThirdPartyReference(updateServicingOrderProducerInput.getThirdPartyReference());

      this.repo.getSvcord().getSvcord15().save(servicingOrderProcedure);
      
    }
  
}
