package com.isw.svcord15.domain.svcord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.messaging.handler.annotation.reactive.PayloadMethodArgumentResolver;
import org.springframework.stereotype.Service;

import com.isw.svcord15.sdk.domain.svcord.entity.CreateServicingOrderProducerInput;
import com.isw.svcord15.sdk.domain.svcord.entity.CustomerReferenceEntity;
import com.isw.svcord15.sdk.domain.svcord.entity.Svcord15;
import com.isw.svcord15.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput;
import com.isw.svcord15.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord15.sdk.domain.svcord.entity.UpdateServicingOrderProducerInputEntity.UpdateServicingOrderProducerInputBuilder;
import com.isw.svcord15.sdk.domain.svcord.service.WithdrawalDomainServiceBase;
import com.isw.svcord15.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord15.sdk.integration.facade.IntegrationEntityBuilder;
import com.isw.svcord15.sdk.integration.partylife.entity.RetrieveLoginInput;
import com.isw.svcord15.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord15.sdk.integration.paymord.entity.PaymentOrderInput;
import com.isw.svcord15.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord15.domain.svcord.command.Svcord15Command;
import com.isw.svcord15.integration.partylife.service.RetrieveLogin;
import com.isw.svcord15.integration.paymord.service.PaymentOrder;
import com.isw.svcord15.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord15.sdk.domain.facade.Repository;

@Service
public class WithdrawalDomainService extends WithdrawalDomainServiceBase {

  @Autowired
  Svcord15Command svcord15Command;

  @Autowired
  PaymentOrder paymentOrder;

  @Autowired
  RetrieveLogin retrieveLogin;

  @Autowired
  IntegrationEntityBuilder integrationEntityBuilder;

  private static final Logger log = LoggerFactory.getLogger(WithdrawalDomainService.class);

  public WithdrawalDomainService(DomainEntityBuilder entityBuilder,  Repository repo) {
  
    super(entityBuilder,  repo);
    
  }
  
  @NewSpan
  @Override
  public com.isw.svcord15.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput execute(com.isw.svcord15.sdk.domain.svcord.entity.WithdrawalDomainServiceInput withdrawalDomainServiceInput)  {

    log.info("WithdrawalDomainService.execute()");
    // TODO: Add your service implementation logic
    RetrieveLoginInput retrieveInput = integrationEntityBuilder
                                  .getPartylife()
                                  .getRetrieveLoginInput()
                                  .setId("test1")
                                  .build();

    RetrieveLoginOutput retrieveLoginOutput = retrieveLogin.execute(retrieveInput);

    if(retrieveLoginOutput.getResult() != "SUCCESS"){
      return null;
    }

    CustomerReferenceEntity customerReferenceEntity = this.entityBuilder.getSvcord().getCustomerReference().build();
    customerReferenceEntity.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    customerReferenceEntity.setAmount(withdrawalDomainServiceInput.getAmount());

    CreateServicingOrderProducerInput createServicingOrderProducerInput = this.entityBuilder.getSvcord().getCreateServicingOrderProducerInput().build();
    createServicingOrderProducerInput.setCustomerReference(customerReferenceEntity);
    Svcord15 svcord15Out = svcord15Command.createServicingOrderProducer(createServicingOrderProducerInput);

    //Payment Order 호출
    PaymentOrderInput paymentOrderInput = integrationEntityBuilder.getPaymord().getPaymentOrderInput().build();

    paymentOrderInput.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    paymentOrderInput.setAmount(withdrawalDomainServiceInput.getAmount());
    paymentOrderInput.setExternalId("SVCORD15");
    paymentOrderInput.setExternalSerive("SVCORD15");
    paymentOrderInput.setPaymentType("PAYMENT_CASH_WITHDRAWALS");

    PaymentOrderOutput paymentOrderOutput = paymentOrder.execute(paymentOrderInput);

    UpdateServicingOrderProducerInput updateServicingOrderProducerInput = this.entityBuilder.getSvcord().getUpdateServicingOrderProducerInput().build();
    updateServicingOrderProducerInput.setUpdateID(svcord15Out.getId().toString());
    updateServicingOrderProducerInput.setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()));

    svcord15Command.updateServicingOrderProducer(svcord15Out, updateServicingOrderProducerInput);

    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = this.entityBuilder.getSvcord()
                                                                  .getWithdrawalDomainServiceOutput()
                                                                  .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()))
                                                                  .setTrasactionId(paymentOrderOutput.getTransactionId())
                                                                  .build();

    return withdrawalDomainServiceOutput;
  }

}
